import sys
sys.path.append('pybrain')

from navigation.environment import NavigationEnvironment
from navigation.task import NavigationTask

from pybrain.rl.agents import LearningAgent
from pybrain.rl.experiments.episodic import EpisodicExperiment
from pybrain.rl.learners.directsearch.enac import ENAC

from pybrain.tools.shortcuts import buildNetwork

from pylab import figure, ion
import numpy as np

# *if updated, also update client.py*
local_ip = '169.234.57.113'
local_port = 15000

# create task
env = NavigationEnvironment(ip=local_ip, port=local_port)
maxsteps = 500
task = NavigationTask(env=env, maxsteps=maxsteps)

# create controller network
net = buildNetwork(task.outdim, task.indim, bias=False)
# net.initParams(0.0)

# create agent
learner = ENAC() # direct search
learner.gd.rprop = True
# RP (???)
learner.gd.deltamin = 1e-4
# BP
learner.gd.alpha = 0.01
learner.gd.momentum = 0.9
agent = LearningAgent(net, learner)
agent.actaspg = False

# create experiment
experiment = EpisodicExperiment(task, agent)

# print weights at beginning
print(agent.module.params)

# episodic
x = 0
#batch = 30 # number of samples per gradient estimate(??? how come we use batch)
batch = 2
while x < 5000:
    experiment.doEpisodes(batch)
    x += batch
    #reward = np.mean(agent.history.getSumOverSequences('reward'))*task.rewardscale
    reward = np.mean(agent.history.getSumOverSequences('reward'))
    print(agent.module.params)
    print(reward)

    agent.learn()
    agent.reset()

if len(sys.argv) > 2:
    agent.history.saveToFile(sys.argv[1], protocol=-1, arraysonly=True)
