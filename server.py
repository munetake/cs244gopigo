import socket, pickle

s = socket.socket()
s.setblocking(1)

#local_ip = '127.0.0.1'
#local_port = 12348
local_ip = '169.234.57.113'
local_port = 15000

print 'Binding local ip and local port to socket'
s.bind((local_ip, local_port))

s.listen(1)
print 'Waiting for connection...'
conn, addr = s.accept()
print 'Got connection from', addr,

while True:
    msg = conn.recv(1024)
    #print msg
    cntr = pickle.loads(msg)
    print 'Message received:', cntr
    
    print 'Sending ack'
    msg = pickle.dumps('<ack>')
    conn.sendall(msg)
        
s.close()
