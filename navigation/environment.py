import socket, pickle
import numpy as np
import time
from time import sleep
from pybrain.tools.networking.udpconnection import UDPServer
from pybrain.rl.environments.environment import Environment

def get_time():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())

def send(sock, obj):
    print get_time(), 'NavigationEnvironment send()'
    msg = pickle.dumps(obj)
    sock.sendall(msg)

def recv(sock):
    print get_time(), 'NavigationEnvironment recv()'
    msg = sock.recv(1024)
    obj = pickle.loads(msg)
    return obj

class NavigationEnvironment(Environment):
    """
    Capsule information for autonomous car:
    State space (continuous):
      s: current speed
      d: 9-dim distance vector focusing on 9 different angles
    Action space (discrete):
      fwd, bwd, left, right
    """
    def __init__(self, ip="127.0.0.1", port="21581"):
        print get_time(), 'initializing NavigationEnvironment'
        
        self.action = 1e-3 * np.random.rand(4)
        self.sensors = [200.0]*13 + [0,0]

        self.sock = socket.socket()
        self.sock.setblocking(1)
        self.sock.bind((ip, port))
        self.sock.listen(1)
        print get_time(), 'waiting for connection'
        self.conn, addr = self.sock.accept()
        print get_time(), 'connected with', addr
        
        self.reset()

    def action_map(argument): 
        switcher = {
            0: "fwd()",
            1: "bwd()",
            2: "left()",
            3: "right()",
        }
        return switcher.get(argument, "nothing")
        
    def step(self):
        print get_time(), 'step()'
        # TODO: communicate with gopigo to move around based on action
        # func(self.action)
        # TODO: commnunicate with gopigo to read sensor information
        # self.sensors = func()
        ai = np.argmax(self.action)
        cmd = action_map(ai)
        send(self.conn, cmd)
        
    def closeSocket(self):
        print get_time(), 'closing socket'
        self.conn.release()
        self.sock.release()

    def reset(self): # no need to do this
        print get_time(), 'resetting NavigationEnvironment'
        pass

    def performAction(self, action):
        print get_time(), 'performing action'
        self.action = action
        self.step()

    #def getSpeed(self):
    #    return self.sensors[0]

    def getDistance(self):
        print get_time(), 'getting distance'
        return self.sensors[:13]

    def getSensors(self):
        print get_time(), 'waiting to receive the sensor information'
        self.sensors = recv(self.conn)
        return self.sensors

    @property
    def indim(self):
        return len(self.action)

    @property
    def outdim(self):
        return len(self.sensors)
