import time, sys, pickle, os, copy
import numpy as np
import cv2
import pdb

opts = {}
opts['camera_id'] = 0
# risky to change camera settings from opencv
#opts['camera_gain']=-1           # not supported
#opts['camera_brightness']=100
#opts['camera_contrast']=5
#opts['camera_saturation']=100
#opts['camera_exposure']=-1.0     # not supported
opts['frame_width'] = 480
opts['frame_height'] = 360
opts['fps'] = 10

def get_time():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())

ref_pts = []
im = []
def click_and_pick(event, x, y, flags, param):
    global ref_pts
    if event == cv2.EVENT_LBUTTONDOWN:
        print 'button down'
        ref_pts = [(x,y)]
    elif event == cv2.EVENT_LBUTTONUP:
        print 'button up'
        ref_pts.append((x,y))
    #if len(ref_pts) == 2:
    #    cv2.rectangle(im, ref_pts[0], ref_pts[1], [0,255,0], 1)

def init_camera():
    global opts
    # set up the camera
    cam = cv2.VideoCapture(opts['camera_id'])
    #cam.set(cv2.cv.CV_CAP_PROP_BRIGHTNESS, opts['camera_brightness'])
    #cam.set(cv2.cv.CV_CAP_PROP_CONTRAST, opts['camera_contrast'])
    #cam.set(cv2.cv.CV_CAP_PROP_SATURATION, opts['camera_saturation'])
    cam.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, opts['frame_width'])
    cam.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, opts['frame_height'])
    #print '-'*30, 'Current setting', '-'*30
    #print 'camera brightness: ', cam.get(cv2.cv.CV_CAP_PROP_BRIGHTNESS)
    #print 'camera contrast: ', cam.get(cv2.cv.CV_CAP_PROP_CONTRAST)
    #print 'camera saturation: ', cam.get(cv2.cv.CV_CAP_PROP_SATURATION)
    #print 'frame width: ', cam.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)
    #print 'frame height: ', cam.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT)
    return cam

def pick_object():
    global im, ref_pts
    cam = init_camera()
    if cam.isOpened == False:
        print 'Error when openning cameras'
        return -1, []
    else:
        print 'Camera initialized'
    # take the picture
    while True:
        # read camera and click
        s, im = cam.read()
        cv2.imshow('Demo', im)
        if cv2.waitKey(20) & 0xFF == 32:
            break
    # denoise the image(too expensive)
    #im = cv2.fastNlMeansDenoisingColored(im,None)
    # pick a rectangle
    cv2.namedWindow('Demo')
    cv2.setMouseCallback('Demo', click_and_pick)
    #ref_pts = [(208, 127), (281, 208)]
    while True:  # for loop for listening to events
        cv2.imshow('Demo', im)
        cv2.waitKey(20)
        if len(ref_pts) >= 2:
            print ref_pts
            break
    cam.release()
    # output the roi
    ref_pts = np.asarray(ref_pts)
    min_x, max_x = min(ref_pts[:,0]), max(ref_pts[:,0])
    min_y, max_y = min(ref_pts[:,1]), max(ref_pts[:,1])
    roi = im[min_y+1:max_y,min_x+1:max_x,:] # ignore plot rect
    return roi

def blob_detector():
    # Setup SimpleBlobDetector parameters.
    params = cv2.SimpleBlobDetector_Params()

    # Change thresholds
    params.filterByColor = True
    params.minThreshold = 127

    # Filter by Area.
    params.filterByArea = True
    params.minArea = 100

    # Filter by Circularity
    params.filterByCircularity = True
    params.minCircularity = 0.01

    # Filter by Convexity
    params.filterByConvexity = False
    #params.minConvexity = 0.87

    # Filter by Inertia
    params.filterByInertia = True
    params.minInertiaRatio = 0.01

    # Create a detector with the parameters
    ver = (cv2.__version__).split('.')
    if int(ver[0]) < 3 :
        detector = cv2.SimpleBlobDetector(params)
    else :
        detector = cv2.SimpleBlobDetector_create(params)

    return detector

def recognize_object(roi):
    # set up the camera
    cam = init_camera()
    if cam.isOpened == False:
        print 'Error when openning cameras'
        return -1, []
    else:
        print 'Camera initialized'
    #
    roi_hsv = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
    roi_hsv_v = roi_hsv.reshape(roi_hsv.shape[0]*roi_hsv.shape[1], roi_hsv.shape[2])
    l_hsv = np.mean(roi_hsv_v,0) - 5*np.std(roi_hsv_v,0)
    h_hsv = np.mean(roi_hsv_v,0) + 5*np.std(roi_hsv_v,0)
    l_hsv[1:] = 30
    h_hsv[1:] = 255
    skern = np.ones((3,3),np.uint8)
    mkern = np.ones((5,5),np.uint8)
    bkern = np.ones((11,11),np.uint8)
    #detector = blob_detector()
    #detector = cv2.SimpleBlobDetector()
    cv2.namedWindow('Demo')
    while True:  # for loop for listening to events
        s,im = cam.read()
        # 0. change colorspace
        hsv = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
        # 1. threshold and binarize
        mask = cv2.inRange(hsv, l_hsv, h_hsv)
        if np.count_nonzero(mask) > 100:
            # 2. remove corner repsonse
            mask = cv2.dilate(mask, skern)
            mask = cv2.erode(mask, mkern)
            mask = cv2.dilate(mask, bkern)
            contours = copy.copy(mask)
            # 3. find connected components and remove small ones
            ctrs, hrc = cv2.findContours(contours,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
            ctr_areas = [cv2.contourArea(ctr) for ctr in ctrs]
            if len(ctr_areas) > 0:
                cv2.imshow('Contours', contours)
                del ctrs[np.argmax(ctr_areas)]
                cv2.drawContours(mask,ctrs,-1,0,cv2.cv.CV_FILLED)

            # 4. estimate the centroid and radius
            yy,xx = np.where(mask==255)
            ymean,xmean = np.mean(yy),np.mean(xx)
            #ystd,xstd = np.std(yy), np.std(xx)
            radius = 1.414*np.mean(np.sqrt((yy-ymean)**2+(xx-xmean)**2))
            radius = np.uint(radius)
            center = (np.uint(xmean), np.uint(ymean))
            #radius = np.uint(3*(ystd+xstd)/2)
            cv2.circle(im,center,radius,(0,255,0),4)
        cv2.imshow('Demo', im)
        cv2.imshow('Mask', mask)
        cv2.waitKey(100)
    cam.release()

roi_path = 'cache/roi.p'
if not os.path.exists('cache'):
    os.makedirs('cache')

if not os.path.exists(roi_path):
    roi = pick_object()
    pickle.dump(roi, open(roi_path, 'wb'))
else:
    roi = pickle.load(open(roi_path, 'rb'))

recognize_object(roi)
