var canvas, ctx;

var World = function(){
  this.agents = [];
  this.clock = 0;
}

World.prototype = {
  tick: function() {
    this.clock ++ ;
    // process eyes (update sensor information)
    for(var i=0, n=this.agents.length; i<n; i++){
      var a = this.agents[i];
      // TODO: waiting to hear back from raspberry pi

      // fill in received sensor information
      for(var ei=0, ne=a.eyes.length; ei<ne; ei++){
        var e = a.eyes[ei];
        e.sensed_proximity = e.max_range;
        e.sensed_type = 0;
      }
    }

    // let the agents behave in the world based on their input
    for(var i=0, n=this.agents.length; i<n; i++){
      this.agents[i].forward();
    }
    // apply outputs of agents on environment
    for(var i=0, n=this.agents.length; i<n; i++){
      var tmp = 1;
    }
    // agents are given the opportunity to learn
    // based on the feedback of their action on environment
    for(var i=0, n=this.agents.length; i<n; i++){
      this.agents[i].backward();
    }

  }
};

var Eye = function(angle){
  this.angle = angle;
  this.max_range = 200;
  this.sensed_proximity = 200; // distance
  this.sensed_type = -1;
};

var Agent = function(){
  /* initialize properties */
  this.actions = [] ;
  this.actions.push('fwd()');
  this.actions.push('bwd()');
  this.actions.push('left()');
  this.actions.push('right()');

  /* initialize eyes */
  this.eyes = [] ;
  for (var k=0; k<=12; k++){
    this.eyes.push(new Eye((15*k)));
  }

  /* initialize the network */
  var num_inputs = 15; // 13 (distance) + 2 (center coordinate)
  var num_actions = 4; // fwd(), bwd(), left(), right()
  var temporal_window = 1; // amount of temporal memory. 0 = agent lives in-the-moment :)
  var network_size = num_inputs*temporal_window +
        num_actions*temporal_window + num_inputs;
  var layer_defs = [];
  layer_defs.push({type:'input', out_sx:1, out_sy:1, out_depth:network_size});
  layer_defs.push({type:'fc', num_neurons: 50, activation:'relu'});
  layer_defs.push({type:'fc', num_neurons: 50, activation:'relu'});
  layer_defs.push({type:'regression', num_neurons:num_actions});
  var tdtrainer_options = {learning_rate:0.001,
                           momentum:0.0,
                           batch_size:64,
                           l2_decay:0.01};
  var opt = {};
  opt.temporal_window = temporal_window;
  opt.experience_size = 30000;
  opt.start_learn_threshold = 1000;
  opt.gamma = 0.7;
  opt.learning_steps_total = 200000;
  opt.learning_steps_burnin = 3000;
  opt.epsilon_min = 0.05;
  opt.epsilon_test_time = 0.05;
  opt.layer_defs = layer_defs;
  opt.tdtrainer_options = tdtrainer_options;
  var brain = new deepqlearn.Brain(num_inputs, num_actions, opt);
  this.brain = brain;

  //
  this.reward_bonus = 0.0;
  this.digestion_signal = 0.0;
};

Agent.prototype = {
  forward: function() {
    var num_eyes = this.eyes.length;
    var input_array = new Array(num_eyes*2);
    for(var i=0; i<num_eyes; i++){
      var e = this.eyes[i];
      input_array[i*2] = 1.0;
      input_array[i*2+1] = 1.0;
      if (e.sensed_type != -1){
        input_array[i*2+e.sensed_type] = e.sensed_proximity/e.max_range;
      }
    }

    // get action from brain
    var actionix = this.brain.forward(input_array);
    var action = this.actions[actionix];
    this.actionix = actionix;

    // send action to raspberry pi
  },

  backward: function() {
    // receive reward from raspberry pi
    var proximity_reward = 0.0;
    var num_eyes = this.eyes.length;
    for(var i=0; i<num_eyes; i++) { // 0 for wall(obstacles), 1 for object
      var e = this.eyes[i];
      // larger proximity against the obstacle, the better
      proximity_reward += e.sensed_type===0?e.sensed_proximity/e.max_range:1.0;
    }
    proximity_reward = proximity_reward / num_eyes;
    proximity_reward = Math.min(1.0, proximity_reward * 2);

    // agents like to go straight forward
    var forward_reward = 0.0;
    if (this.actionix === 0 && proximity_reward > 0.75)
      forward_reward = 0.1 * proximity_reward;

    // TODO: agents like to see big object


    // sum up all reward
    var reward = proximity_reward + forward_reward;

    // pass to brain for learning
    this.brain.backward(reward);
  }
};

/* visualization functions */
function draw_net() {
  var canvas = document.getElementById("net_canvas");
  var ctx = canvas.getContext("2d");
  var W = canvas.width;
  var H = canvas.height;
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  var L = w.agents[0].brain.value_net.layers;
  var dx = (W - 50)/L.length;
  var x = 10;
  var y = 40;
  ctx.font="12px Verdana";
  ctx.fillStyle = "rgb(0,0,0)";
  ctx.fillText("Value Function Approximating Neural Network:", 10, 14);
  for(var k=0;k<L.length;k++) {
    if(typeof(L[k].out_act)==='undefined') continue; // maybe not yet ready
    var kw = L[k].out_act.w;
    var n = kw.length;
    var dy = (H-50)/n;
    ctx.fillStyle = "rgb(0,0,0)";
    ctx.fillText(L[k].layer_type + "(" + n + ")", x, 35);
    for(var q=0;q<n;q++) {
      var v = Math.floor(kw[q]*100);
      if(v >= 0) ctx.fillStyle = "rgb(0,0," + v + ")";
      if(v < 0) ctx.fillStyle = "rgb(" + (-v) + ",0,0)";
      ctx.fillRect(x,y,10,10);
      y += 12;
      if(y>H-25) { y = 40; x += 12};
    }
    x += 50;
    y = 40;
  }
}

var reward_graph = new cnnvis.Graph();
function draw_stats() {
  var canvas = document.getElementById("vis_canvas");
  var ctx = canvas.getContext("2d");
  var W = canvas.width;
  var H = canvas.height;
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  var a = w.agents[0];
  var b = a.brain;
  var netin = b.last_input_array;
  ctx.strokeStyle = "rgb(0,0,0)";
  //ctx.font="12px Verdana";
  //ctx.fillText("Current state:",10,10);
  ctx.lineWidth = 10;
  ctx.beginPath();
  for(var k=0,n=netin.length;k<n;k++) {
    ctx.moveTo(10+k*12, 120);
    ctx.lineTo(10+k*12, 120 - netin[k] * 100);
  }
  ctx.stroke();

  if(w.clock % 200 === 0) {
    reward_graph.add(w.clock/200, b.average_reward_window.get_average());
    var gcanvas = document.getElementById("graph_canvas");
    reward_graph.drawSelf(gcanvas);
  }
}

// Tick the world
function tick() {
  w.tick();
  if(!skipdraw || w.clock % 50 === 0) {
    // draw();
    draw_stats();
    draw_net();
  }
}

/* helper functions */
function savenet() {
  var j = w.agents[0].brain.value_net.toJSON();
  var t = JSON.stringify(j);
  document.getElementById('tt').value = t;
}

function loadnet() {
  var t = document.getElementById('tt').value;
  var j = JSON.parse(t);
  w.agents[0].brain.value_net.fromJSON(j);
  stoplearn(); // also stop learning
  gonormal();
}

function startlearn() {
  w.agents[0].brain.learning = true;
}
function stoplearn() {
  w.agents[0].brain.learning = false;
}

function reload() {
  w.agents = [new Agent()]; // this should simply work. I think... ;\
  reward_graph = new cnnvis.Graph(); // reinit
}

var simspeed = 2;
function gofast() {
  window.clearInterval(current_interval_id);
  current_interval_id = setInterval(tick, 0);
  skipdraw = false;
  simspeed = 2;
}

var w; // global world object
var current_interval_id;
var skipdraw = false;

function start() {
  canvas = document.getElementById("canvas");
  ctx = canvas.getContext("2d");

  w = new World();
  w.agents = [new Agent()];

  gofast();
}
